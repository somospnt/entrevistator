DROP TABLE IF EXISTS entrevistado;

CREATE TABLE entrevistado (
    id BIGINT IDENTITY PRIMARY KEY,
    nombre VARCHAR(50), 
    apellido VARCHAR(50),
    edad INT
);

INSERT INTO entrevistado VALUES (1,'Carolina','Barbiero',0);
INSERT INTO entrevistado VALUES (2,'Eduardo','Hernandez',0);
INSERT INTO entrevistado VALUES (3,'Diego','Gomez',0);
INSERT INTO entrevistado VALUES (4,'Emiliano','Ramieri',0);
INSERT INTO entrevistado VALUES (5,'Emiliano','Tebes',0);
INSERT INTO entrevistado VALUES (6,'Juan','Cernadas',0);
INSERT INTO entrevistado VALUES (7,'Federico','Miras',0);
INSERT INTO entrevistado VALUES (8,'Laureano','Clausi',0);
INSERT INTO entrevistado VALUES (9,'Leonardo','De Seta',0);
INSERT INTO entrevistado VALUES (10,'Manuel','Sanchez Avalos',0);
INSERT INTO entrevistado VALUES (11,'Mariano','Sola',0);
INSERT INTO entrevistado VALUES (12,'Martin','Bertucelli',0);
INSERT INTO entrevistado VALUES (13,'Matias','Zamorano',0);
INSERT INTO entrevistado VALUES (14,'Pablo','Rivero',0);
INSERT INTO entrevistado VALUES (15,'Roman','Glombovsky',0);
INSERT INTO entrevistado VALUES (16,'William','Herrera',0);
INSERT INTO entrevistado VALUES (17,'Francisco','Disalvo',0);
