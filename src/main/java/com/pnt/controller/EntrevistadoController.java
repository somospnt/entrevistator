package com.pnt.controller;

import com.pnt.service.EntrevistadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EntrevistadoController {
    
    @Autowired
    private EntrevistadoService entrevistadoService;
    
    @RequestMapping("/")
    public String entrevistados(Model model){  
        model.addAttribute("entrevistados", entrevistadoService.buscarTodos());
        return "entrevistados";
    }  
}
