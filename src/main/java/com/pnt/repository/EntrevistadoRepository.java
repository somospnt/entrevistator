package com.pnt.repository;

import com.pnt.domain.Entrevistado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistadoRepository extends JpaRepository<Entrevistado, Long> {

}
