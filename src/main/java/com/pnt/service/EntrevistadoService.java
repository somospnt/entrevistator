package com.pnt.service;

import com.pnt.domain.Entrevistado;
import java.util.List;

public interface EntrevistadoService  {
    
    List<Entrevistado> buscarTodos();
    
}
