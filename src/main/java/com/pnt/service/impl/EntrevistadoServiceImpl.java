package com.pnt.service.impl;

import com.pnt.domain.Entrevistado;
import com.pnt.repository.EntrevistadoRepository;
import com.pnt.service.EntrevistadoService;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EntrevistadoServiceImpl implements EntrevistadoService {

    @Autowired
    private EntrevistadoRepository entrevistadoRepository;
            
    @Override
    public List<Entrevistado> buscarTodos() {
       return entrevistadoRepository.findAll();
    }
    
}
