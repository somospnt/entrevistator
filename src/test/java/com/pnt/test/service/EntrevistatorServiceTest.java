package com.pnt.test.service;

import com.pnt.domain.Entrevistado;
import com.pnt.service.EntrevistadoService;
import com.pnt.test.EntrevistatorApplicationTests;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class EntrevistatorServiceTest extends EntrevistatorApplicationTests{

    @Autowired
    private EntrevistadoService entrevistadoService;

    @Test
    public void buscarTodos_entrevistadosExisten_devuelveListaEntrevistados() {

        List<Entrevistado> entrevistados = entrevistadoService.buscarTodos();
        assertNotNull(entrevistados);
        assertEquals("Carolina", entrevistados.get(0).getNombre());
        assertEquals(17, entrevistados.size());
    }

}
