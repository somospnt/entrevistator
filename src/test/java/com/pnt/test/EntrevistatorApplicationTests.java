package com.pnt.test;

import com.pnt.EntrevistatorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EntrevistatorApplication.class)
@WebAppConfiguration
public abstract class EntrevistatorApplicationTests {

	@Test
	public void contextLoads() {
	}

}
